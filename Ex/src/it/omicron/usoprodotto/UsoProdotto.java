package it.omicron.usoprodotto;

public class UsoProdotto {
	public static void main(String[] args) {
		Prodotto p1 = new Prodotto(1, "Mela", 7.5);
		Prodotto p2 = new Prodotto(2, "Arancia", 4.5);
		double med = (p1.getPrezzo() + p2.getPrezzo()) / 2;
		if (p1.getPrezzo() > p2.getPrezzo()) {
			System.out.println("Codice: " + p2.getCod_Prodotto() + ", Descrizione: " + p2.getDescrizione()
					+ ", Prezzo: " + p2.getPrezzo());
			System.out.println("Codice: " + p1.getCod_Prodotto() + ", Descrizione: " + p1.getDescrizione()
					+ ", Prezzo: " + p1.getPrezzo());
		} else {
			System.out.println("Codice: " + p1.getCod_Prodotto() + ", Descrizione: " + p1.getDescrizione()
					+ ", Prezzo: " + p1.getPrezzo());
			System.out.println("Codice: " + p2.getCod_Prodotto() + ", Descrizione: " + p2.getDescrizione()
					+ ", Prezzo: " + p2.getPrezzo());
		}
		for (int i = 0; i < 10; i++) {

			System.out.println("" + i);
		}
	}
}