package it.omicron.usoprodotto;

public class Prodotto {
	private int Cod_Prodotto;
	private String Descrizione;
	private double Prezzo;

	public int getCod_Prodotto() {
		return Cod_Prodotto;
	}

	public String getDescrizione() {
		return Descrizione;
	}

	public double getPrezzo() {
		return Prezzo;
	}

	public Prodotto() {
	}

	public Prodotto(int Cod_Prodotto, String Descrizione, double Prezzo) {
		this.Cod_Prodotto = Cod_Prodotto;
		this.Descrizione = Descrizione;
		this.Prezzo = Prezzo;
	}
}