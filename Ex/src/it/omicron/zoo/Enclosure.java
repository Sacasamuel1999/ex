package it.omicron.zoo;

public class Enclosure {
	int id;
	int idGuardian;
	int extension;

	public Enclosure() {
	}

	public Enclosure(int id, int idGuardian, int extension) {
		this.id = id;
		this.idGuardian = idGuardian;
		this.extension = extension;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdGuardian() {
		return idGuardian;
	}

	public void setIdGuardian(int idGuardian) {
		this.idGuardian = idGuardian;
	}

	public int getExtension() {
		return extension;
	}

	public void setExtension(int extension) {
		this.extension = extension;
	}

	/*
	 * @Override public String toString() { return "Enclosure [id=" + id +
	 * ", idGuardian=" + idGuardian + ", extension=" + extension + "]"; }
	 */
}
