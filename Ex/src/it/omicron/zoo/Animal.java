package it.omicron.zoo;

public class Animal {
	private int age;
	private int id;
	private int idEnclosure;
	private int weight;
	private String name;

	public Animal() {
	}

	public Animal(int age, int id, int idEnclosure, int weight, String name) {
		this.age = age;
		this.id = id;
		this.idEnclosure = idEnclosure;
		this.weight = weight;
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdEnclosure() {
		return idEnclosure;
	}

	public void setIdEnclosure(int idEnclosure) {
		this.idEnclosure = idEnclosure;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

/*	@Override
	public String toString() {
		return "Animal [age=" + age + ", id=" + id + ", idEnclosure=" + idEnclosure + ", weight=" + weight + ", name="
				+ name + "]"; 
	} */
}