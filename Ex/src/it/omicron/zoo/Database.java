package it.omicron.zoo;

import it.omicron.zoo.Animal;
import it.omicron.zoo.Guardian;
import it.omicron.zoo.Enclosure;
import java.util.ArrayList;

public class Database {
	private static Database singleton = null;

	public static Database getInstance() {
		if (singleton == null) {
			singleton = new Database();
		}
		return singleton;
	}

	ArrayList<Animal> l_o_a = new ArrayList<Animal>();
	int s_o_a = 0;

	public synchronized void aggiungiAnimale(Animal p) {
		s_o_a++;
		p.setId(s_o_a);
		l_o_a.add(p);
	}

	public synchronized ArrayList<Animal> getl_o_a() {
		return l_o_a;
	}

	ArrayList<Guardian> l_o_g = new ArrayList<Guardian>();
	int s_o_g = 0;

	public synchronized void aggiungiCustode(Guardian c) {
		s_o_g++;
		c.setId(s_o_g);
		l_o_g.add(c);
	}

	public synchronized ArrayList<Guardian> getl_o_g() {
		return l_o_g;
	}

	ArrayList<Enclosure> l_o_e = new ArrayList<Enclosure>();
	int s_o_e = 0;

	public synchronized void aggiungiRecinto(Enclosure r) {
		s_o_e++;
		r.setId(s_o_e);
		l_o_e.add(r);
	}

	public synchronized ArrayList<Enclosure> getl_o_e() {
		return l_o_e;
	}
}