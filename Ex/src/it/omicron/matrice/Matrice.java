package it.omicron.matrice;

import java.util.Scanner;

public class Matrice {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il numero delle righe: ");
		int righe = input.nextInt();
		System.out.println("Inserisci il numero delle colonne: ");
		int colonne = input.nextInt();
		int M[][] = new int[righe][colonne];
		int num_neg = 0;
		int num_null = 0;
		int num_pos = 0;
		for (int i = 0; i < righe; i++) {
			for (int j = 0; j < colonne; j++) {
				System.out.println("Inserisci gli elementi: ");
				M[i][j] = input.nextInt();
				if (M[i][j] == 0) {
					num_null++;
				} else if (M[i][j] < 0) {
					num_neg++;
				} else {
					num_pos++;
				}
			}
		}
		System.out.println("I valori positivi sono: " + num_pos);
		System.out.println("I valori nulli sono: " + num_null);
		System.out.println("I valori negativi sono: " + num_neg);
		System.out.println("I valori della matrice: ");
		for (int i = 0; i < righe; i++) {
			for (int j = 0; j < colonne; j++) {
				System.out.println(M[i][j]);
			}
		}
	}
}
