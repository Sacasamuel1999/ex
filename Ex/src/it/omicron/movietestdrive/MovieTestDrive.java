package it.omicron.movietestdrive;

public class MovieTestDrive {
	public static void main(String[] args) {
		Movie one = new Movie();
		one.title = "Gone with the Stock";
		one.genre = "Tragic";
		one.raiting = -2;
		Movie two = new Movie();
		two.title = "Lost in Cubicle Space";
		two.genre = "Comedy";
		two.raiting = 5;
		two.playIt();
		Movie three = new Movie();
		three.title = "Byte club";
		three.genre = "Tragic but ultimately uplifting";
		three.raiting = 127;
	}
}