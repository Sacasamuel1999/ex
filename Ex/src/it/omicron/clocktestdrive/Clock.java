package it.omicron.clocktestdrive;

class Clock {
	String time;

	void setTime(String t) {
		time = t;
	}

	String getTime() {
		return time;
	}
}