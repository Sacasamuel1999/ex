package it.omicron.biblioteca;

public class Copy {
	private int id;
    private int idBook;
    private int idPublishing_home;
    private boolean available;

    public Copy() {
    }

    public Copy(int idBook, int idPublishing_home) {
        this.idBook = idBook;
        this.idPublishing_home = idPublishing_home;
        this.available = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public int getIdPublishing_home() {
        return idPublishing_home;
    }

    public void setIdPublishing_home(int idPublishing_home) {
        this.idPublishing_home = idPublishing_home;
    }
    
    public void restituito(){
        this.available = true;
    }
    
    public void presoInPrestito(){
        this.available = false;
    }

    public boolean isDisponibile() {
        return available;
    }

}