package it.omicron.biblioteca;

import data.Database;
import entita.Copia;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Add_copy", urlPatterns = { "/Add_copy" })
public class Add_copy extends HttpServlet {

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		int idBook = Integer.parseInt(request.getParameter("Book: "));
		int idPublishing_house = Integer.parseInt(request.getParameter("Publishing house: "));
		Copy copia = new Copy(idBook, idPublishing_house);
		Database data = Database.getIstance();
		data.AggiungiCopia(copia);
		HttpSession session = request.getSession();
		session.setAttribute("List of book: ", data.getListaLibri());
		session.setAttribute("List of author: ", data.getListaAutori());
		session.setAttribute("List of publishing house: ", data.getListaCaseEditrici());
		session.setAttribute("List of copy: ", data.getListaCopie());
		session.setAttribute("List of signed up: ", data.getListaIscritti());
		session.setAttribute("List of loan: ", data.getListaPrestiti());
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.html");
		dispatcher.forward(request, response);
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}