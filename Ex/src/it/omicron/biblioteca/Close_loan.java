package it.omicron.biblioteca;

import data.Database;
import entita.Prestito;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ChiudiPrestito", urlPatterns = { "/ChiudiPrestito" })
public class Close_loan extends HttpServlet {

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
		int idSigned_up = Integer.parseInt(request.getParameter("Signed_up: "));
		String day = request.getParameter("Day: ");
		String month = request.getParameter("Month: ");
		String year = request.getParameter("Year: ");
		String E_d = day + "/" + month + "/" + year;
		Database db = Database.getIstance();
		Loan p = db.getl_o_l().get(idLoan - 1);
		int idI = p.getl_o_s_u();
		int idC = p.getIdCopy();
		p.setEnd_date(E_d);
		db.restituzioneLibro(idI, idC);
		HttpSession session = request.getSession();
		session.setAttribute("List of book: ", db.getListaLibri());
		session.setAttribute("List of author: ", db.getListaAutori());
		session.setAttribute("List of publishing house: ", db.getListaCaseEditrici());
		session.setAttribute("List of copy: ", db.getl_o_c());
		session.setAttribute("List of signed up: ", db.getl_o_s_u());
		session.setAttribute("List of loan: ", db.getl_o_l());
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.html");
		dispatcher.forward(request, response);
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	// + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>
}