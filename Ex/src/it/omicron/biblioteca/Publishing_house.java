package it.omicron.biblioteca;

public class Publishing_house {
	private int id;
    private String nome;

    public Publishing_house() {
    }

    public Publishing_house(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}