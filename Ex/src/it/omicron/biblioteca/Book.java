package it.omicron.biblioteca;

public class Book {
	private int id;
	private String name;
	private int idAuthor;

	public Book() {
	}

	public Book(String name, int idAuthor) {
		this.name = name;
		this.idAuthor = idAuthor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getIdAutore() {
		return idAuthor;
	}

	public void setIdAutore(int idAuthor) {
		this.idAuthor = idAuthor;
	}
}