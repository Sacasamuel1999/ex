package it.omicron.biblioteca;

import entita.Autore;
import entita.CasaEditrice;
import entita.Copia;
import entita.Iscritto;
import entita.Libro;
import entita.Prestito;
import java.util.ArrayList;

public class Database {

	private static Database singleton = null;

	public static Database getIstance() {
		if (singleton == null) {
			singleton = new Database();
		}
		return singleton;
	}

	private ArrayList<Book> l_o_b = new ArrayList();
	private ArrayList<Author> l_o_a = new ArrayList();
	private ArrayList<Publishing_house> l_o_p_h = new ArrayList();
	private ArrayList<Copy> l_o_c = new ArrayList();
	private ArrayList<Signed_up> l_o_s_u = new ArrayList();
	private ArrayList<Loan> l_o_l = new ArrayList();
	private int cBook = 0;
	private int cAuthor = 0;
	private int cPublishing_house = 0;
	private int cCopy = 0;
	private int cSigned_up = 0;
	private int cLoan = 0;

	public ArrayList<Book> getListaLibri() {
		return l_o_b;
	}

	public ArrayList<Author> getListaAutori() {
		return l_o_a;
	}

	public ArrayList<Publishing_house> getListaCaseEditrici() {
		return l_o_p_h;
	}

	public ArrayList<Copy> getl_o_c() {
		return l_o_c;
	}

	public ArrayList<Signed_up> getl_o_s_u() {
		return l_o_s_u;
	}

	public ArrayList<Loan> getl_o_l() {
		return l_o_l;
	}

	public void Add_book(Book l) {
		cBook++;
		l.setId(cBook);
		l_o_b.add(l);
	}

	public void Add_author(Author a) {
		cAuthor++;
		a.setId(cAuthor);
		l_o_a.add(a);
	}

	public void AddPublishing_house(Publishing_house c) {
		cPublishing_house++;
		c.setId(cPublishing_house);
		l_o_p_h.add(c);
	}

	public void Add_copy(Copy c) {
		cCopy++;
		c.setId(cCopy);
		l_o_c.add(c);
	}

	public void AddSigned_up(Signed_up i) {
		cSigned_up++;
		i.setId(cSigned_up);
		l_o_s_u.add(i);
	}

	public void Add_loan(Loan p) {
		cLoan++;
		p.setId(cLoan);
		l_o_l.add(p);
	}

	public boolean Controll(int idI, int idC) {
		boolean totale = false;
		if (l_o_c.get(idC - 1).isDisponibile() && l_o_s_u.get(idI - 1).isLibero()) {
			totale = true;
		}
		return totale;
	}

	public void PresaLibro(int idI, int idC) {
		Signed_up i = l_o_s_u.get(idI - 1);
		Copy a = l_o_c.get(idC - 1);
		a.presoInPrestito();
		i.presaLibro();
	}

	public void restituzioneLibro(int idI, int idC) {
		Signed_up i = l_o_s_u.get(idI - 1);
		Copy a = l_o_c.get(idC - 1);
		a.restituito();
		i.restituzioneLibro();
	}

}