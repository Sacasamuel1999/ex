package it.omicron.biblioteca;

public class Loan {
	private int id;
	private int idSigned_up;
	private int idCopy;
	private String Start_date;
	private String End_date;

	public Loan() {
	}

	public Loan(int idSigned_up, int idCopy, String Start_date) {
		this.idSigned_up = idSigned_up;
		this.idCopy = idCopy;
		this.Start_date = Start_date;
		this.End_date = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdSigned_up() {
		return idSigned_up;
	}

	public void setIdSigned_up(int idSigned_up) {
		this.idSigned_up = idSigned_up;
	}

	public int getIdCopy() {
		return idCopy;
	}

	public void setIdCopy(int idCopy) {
		this.idCopy = idCopy;
	}

	public String getStart_date() {
		return Start_date;
	}

	public void setStart_date(String Start_date) {
		this.Start_date = Start_date;
	}

	public String getEnd_date() {
		return End_date;
	}

	public void setDataFine(String End_date) {
		this.End_date = End_date;
	}
}