package it.omicron.biblioteca;
 
import data.Database;
import entita.Prestito;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "Request_loan", urlPatterns = {"/Request_loan"})
public class Request_loan extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        int idSigned_up = Integer.parseInt(request.getParameter("Signed up: "));
        int idCopy = Integer.parseInt(request.getParameter("Copy: "));
        String Start_date = request.getParameter("Day 1: ") + "/" + request.getParameter("Month 1: ") + "/" + request.getParameter("Year 1: ");
        Loan prestito = new Loan (idSigned_up, idCopy, Start_date);
        Database data = Database.getIstance();
        if (data.controll(idSigned_up, idCopy)) {
            data.AggiungiPrestito(prestito);
            data.PresaLibro(idSigned_up, idCopy);
        }
        HttpSession sessione = request.getSession();
        sessione.setAttribute("List of book: ", data.getListaLibri());
        sessione.setAttribute("List of author: ", data.getListaAutori());
        sessione.setAttribute("List of publishing home: ", data.getListaCaseEditrici());
        sessione.setAttribute("List of copy", data.getListaCopie());
        sessione.setAttribute("List of signed up: ", data.getListaIscritti());
        sessione.setAttribute("List of loan: ", data.getListaPrestiti());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.html");
        dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}