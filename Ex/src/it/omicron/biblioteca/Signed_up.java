package it.omicron.biblioteca;

public class Signed_up {
	private int id;
    private String nome;
    private String cognome;
    private int age;
    private boolean libero;

    public Signed_up() {
    }

    public Signed_up(String nome, String cognome, int age) {
        this.nome = nome;
        this.cognome = cognome;
        this.age = age;
        this.libero = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public int age() {
        return age;
    }

    public void age(int age) {
        this.age = age;
    }
    
    public void presaLibro(){
        this.libero = false;
    }
    
    public void restituzioneLibro(){
        this.libero = true;
    }
    
    public boolean controlloDisponibilita(){
        return libero;
    }

    public boolean isLibero() {
        return libero;
    }
}