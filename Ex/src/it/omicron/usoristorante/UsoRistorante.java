package it.omicron.usoristorante;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class UsoRistorante {
	public static void main(String[] args) {
		Ristorante[] ris = new Ristorante[5];
		ArrayList<Ristorante> r = new ArrayList();
		HashMap<String, Ristorante> hm = new HashMap();
		ris[0] = new Ristorante("Il Cambio", "Piemonte", 8.0);
		ris[1] = new Ristorante("Tre Da Tre", "Piemonte", 6.0);
		ris[2] = new Ristorante("Le Due Isole", "piemonte", 4.0);
		ris[3] = new Ristorante("Le Streghe", "Lazio", 5.0);
		ris[4] = new Ristorante("Trattoria Da Mara", "Friuli Venezia-Giulia", 5.5);
		for (Ristorante list : ris) {
			if (list.getVoto() < 6) {
				r.add(list);
			} else {
				hm.put(list.getNome(), list);
			}
		}
		double n_insuf = 0, n_suf = 0;
		int c1 = 0, c2 = 0;
		for (Ristorante s : r) {
			if (s.getNome().equals("Piemonte")) {
				n_insuf += s.getVoto();
				c1++;
			}
			System.out.println(s);

		}
		for (Map.Entry<String, Ristorante> p : hm.entrySet()) {
			String c = p.getKey();
			Ristorante o = p.getValue();
			System.out.println(c + " " + o);
		}
	}
}