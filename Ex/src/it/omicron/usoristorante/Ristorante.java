package it.omicron.usoristorante;

public class Ristorante {
	private String nome, regione;
	private double voto;

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setRegione(String regione) {
		this.regione = regione;
	}

	public void setVoto(double voto) {
		this.voto = voto;
	}

	public String getNome() {
		return nome;
	}

	public String getRegione() {
		return regione;
	}

	public double getVoto() {
		return voto;
	}

	public Ristorante() {
	}

	public Ristorante(String nome, String regione, double voto) {
		this.nome = nome;
		this.regione = regione;
		this.voto = voto;
	}

	@Override
	public String toString() {
		return "Ristorante{" + "nome=" + nome + ", regione=" + regione + ", voto=" + voto + '}';
	}
}