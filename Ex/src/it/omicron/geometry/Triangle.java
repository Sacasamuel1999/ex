package it.omicron.geometry;

public abstract class Triangle {
	public Triangle(TriangleType type) {
		this.type = type;
		arrangeParts();
	}

	private void arrangeParts() {

	}

	protected abstract void construct();

	private TriangleType type = null;

	public TriangleType getType() {
		return type;
	}

	public void setType(TriangleType type) {
		this.type = type;
	}

	public abstract void getPerimeter(double side_one, double side_two, double base);

	public abstract void getArea(double base, double height);

}