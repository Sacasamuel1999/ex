package it.omicron.geometry;

public class Scalene extends Triangle {

	public Scalene() {
		super(TriangleType.SCALENE);
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Your triangle is scalene.");
	}

	@Override
	public void getPerimeter(double side_one, double side_two, double base) {
		double perimeter = side_one + side_two + base;
		System.out.println("The value of the perimeter is: " + perimeter);
	}

	@Override
	public void getArea(double base, double height) {
		double area = (base * height) / 2;
		System.out.println("The value of the area is: " + area);
	}
}