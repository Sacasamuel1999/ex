package it.omicron.geometry;

public class Equilateral extends Triangle {

	public Equilateral() {
		super(TriangleType.EQUILATERAL);
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Your triangle is equilater.");
	}

	@Override
	public void getPerimeter(double side_one, double side_two, double base) {
		if (side_one == side_two && side_one == base && side_two == base) {
			double perimeter = side_one * 3;
			System.out.println("The value of the perimeter is: " + perimeter);
		}
	}

	@Override
	public void getArea(double base, double height) {
		double area = (base * height) / 2;
		System.out.println("The value of the area is: " + area);
	}
}