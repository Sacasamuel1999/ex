package it.omicron.geometry;

import java.util.Scanner;

public class TestFactoryPattern {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		try {
			System.out.println("You must insert the value of the first side: ");
			double side_one = input.nextDouble();
			if (side_one == 0)
				throw new NDLUAZException();
			System.out.println("You must insert the value of the second side: ");
			double side_two = input.nextDouble();
			if (side_two == 0)
				throw new NDLUAZException();
			System.out.println("You must insert the value of the base: ");
			double base = input.nextDouble();
			System.out.println("You must insert the value of the height: ");
			double height = input.nextDouble();

			TriangleFactory tf = new TriangleFactory();

			if (side_one == side_two && side_one == base && side_two == base) {
				tf.buildTriangle(TriangleType.EQUILATERAL);
			} else if (side_one == side_two || side_one != base) {
				System.out.println(TriangleFactory.buildTriangle(TriangleType.ISOSCELES));
				tf.buildTriangle(TriangleType.ISOSCELES);
			} else {
				tf.buildTriangle(TriangleType.SCALENE);
			}
		} catch (NDLUAZException e) {
			System.out.println(e.getMessage());
		}
	}
}