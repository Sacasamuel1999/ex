package it.omicron.geometry;

public class TriangleFactory {
	public static Triangle buildTriangle(TriangleType model) {
		Triangle triangle = null;
		switch (model) {
		case EQUILATERAL:
			triangle = new Equilateral();
			break;

		case ISOSCELES:
			triangle = new Isosceles();
			break;

		case SCALENE:
			triangle = new Scalene();
			break;

		default:
			// throw some exception
			break;
		}
		return triangle;
	}
}