package it.omicron.geometry;

public class NDLUAZException extends Exception {
	public NDLUAZException() {
		super("The side can't be equals to zero");
	}

	public NDLUAZException(String messaggio) {
		super(messaggio);
	}

}