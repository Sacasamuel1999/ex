package it.omicron.geometry;

public class Isosceles extends Triangle {

	public Isosceles() {
		super(TriangleType.ISOSCELES);
		construct();
	}

	@Override
	protected void construct() {
		System.out.println("Your triangle is isosceles.");
	}

	@Override
	public void getPerimeter(double side_one, double side_two, double base) {
		if (side_one == side_two || side_one != base) {
			double perimeter = (side_one * 2) + base;
			System.out.println("The value of the perimeter is: " + perimeter);
		}
	}

	@Override
	public void getArea(double base, double height) {
		double area = (base * height) / 2;
		System.out.println("The value of the area is: " + area);
	}
}