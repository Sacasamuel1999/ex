package it.omicron.geometry;

public enum TriangleType {
	EQUILATERAL, ISOSCELES, SCALENE;
}