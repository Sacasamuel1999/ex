package it.omicron.servletrain;

public class Cell {
	private boolean water;
	private boolean round;
	private int height;
	
	public Cell() {
		this.water = false;
		this.round = false;
		this.height = (int) (Math.random() * 20);
	}

	public boolean isWater() {
		return water;
	}

	public void setWater(boolean water) {
		this.water = water;
	}

	public boolean isRound() {
		return round;
	}

	public void setRound(boolean round) {
		this.round = round;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
}
