package it.omicron.servletrain;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;

@WebServlet(urlPatterns = { "/servletRain" })
public class servletRain extends HttpServlet {

	static Cell table[][] = new Cell[25][25];
	static int column;
	static int lap;
	static int row;

	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");

		// to understand where the water goes
		int k = table.length - 1;
		lap = Integer.parseInt(request.getParameter("giro"));
		;
		row = Integer.parseInt(request.getParameter("riga"));
		column = Integer.parseInt(request.getParameter("colonna"));
		int count = 0;
		if (lap == 0) {
			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table.length; j++) {
					table[i][j] = new Cell();
				}
			}
			table[row][column].setWater(true);
			table[row][column].setRound(true);
		}
		int x = ((table.length) * (table.length));
		String colore[] = new String[x];
		do {
			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table.length; j++) {
					if (table[i][j].isWater() == true && table[i][j].isRound() == true) {
						if (i < k && j < k && i > 0 && j > 0) { /* normal */
							if (table[(i - 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j - 1)].setWater(true);
							}
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[(i - 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j + 1)].setWater(true);
							}
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
							if (table[(i + 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
							if (table[(i + 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j + 1)].setWater(true);
							}
						}
						if (i == 0 && j == 0) { /* upper left corner */
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
							if (table[(i + 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j + 1)].setWater(true);
							}
						}
						if (i == 0 && j == k) { /* upper right corner */
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
						}
						if (i == k && j == 0) { /* lower left corner */
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[(i - 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j + 1)].setWater(true);
							}
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
						}
						if (i == k && j == k) { /* lower right corner */
							if (table[(i - 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j - 1)].setWater(true);
							}
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
						}
						if (j == 0 && i != 0 && i != k) { /* left side */
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[(i - 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j + 1)].setWater(true);
							}
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
							if (table[(i + 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j + 1)].setWater(true);
							}
						}
						if (j == k && i != 0 && i != k) { /* right side */
							if (table[(i - 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j - 1)].setWater(true);
							}
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
						}
						if (i == 0 && j != 0 && j != k) { /* high side */
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
							if (table[(i + 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j - 1)].setWater(true);
							}
							if (table[(i + 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][j].setWater(true);
							}
							if (table[(i + 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i + 1)][(j + 1)].setWater(true);
							}
						}
						if (i == k && j != 0 && j != k) { /* low side */
							if (table[(i - 1)][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j - 1)].setWater(true);
							}
							if (table[(i - 1)][j].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][j].setWater(true);
							}
							if (table[(i - 1)][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[(i - 1)][(j + 1)].setWater(true);
							}
							if (table[i][(j - 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j - 1)].setWater(true);
							}
							if (table[i][(j + 1)].getHeight() <= table[i][j].getHeight()) {
								table[i][(j + 1)].setWater(true);
							}
						}
					}
				}
			}

			/* to change the color where there's the water */
			for (int i = 0; i < x; i++) {
				colore[i] = "#cc9900";
			}
			int cc = 0;
			for (int i = 0; i < table.length; i++) {
				for (int j = 0; j < table.length; j++) {
					if (table[i][j].isAcqua() == true) {
						table[i][j].setTurno(true);
						colore[cc] = "#0000cc";
					}
					cc++;
				}
			}
			count++;
		} while (count <= lap);
		/* to create the table in HTML */
		String tab = "";
		int fc = 0;
		for (int i = 0; i < table.length; i++) {
			tab = tab + "<tr>";
			for (int j = 0; j < table.length; j++) {
				tab = tab + "<td bgcolor=" + colore[fc] + ">" + table[i][j].getHeight() + "</td>";
				fc++;
			}
			tab = tab + "</tr>";
		}

		try (PrintWriter out = response.getWriter()) {
			/* TODO output your page here. You may use following sample code. */
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<title>Ecco la Pioggia</title>");
			out.println("<style>");
			out.println("table, td {");
			out.println("border: 1px solid black;");
			out.println("border-collapse: collapse;}");
			out.println("td{");
			out.println("padding: 5 px;");
			out.println("text-align: center;}");
			out.println("</style>");
			out.println("</head>");
			out.println("<body style='background-color: #66ff99'>");
			out.println("<center><h1 style='color: #000066'>");
			out.println("Ta da! <br>");
			out.println("</h1>");
			out.println("<p>");
			out.println("Cliccare su continua per vedere come di diffonde dopo la pioggia. <br>");
			out.println("Se mette zero nell'input giro, prima di continuare, <br>");
			out.println("la tabella cambierà in una nuova, con i valori riga e colonna inseriti. <br>");
			out.println("</p>");
			out.println("<br><br>");
			out.println("<form action=servletPioggia>");
			out.println("Riga: <input name=riga value=" + (row) + " />");
			out.println("Colonna: <input name=colonna value=" + (column) + " />");
			out.println("Giro: <input name=giro value=" + (lap + 1) + " />");
			out.println("<input type=submit value=Continua />");
			out.println("</form>");
			out.println("<br><br>");
			out.println("<center><table style=width:10%>");
			out.println(tab);
			out.println("</table></center>");
			out.println("</body>");
			out.println("</html>");
		}
	}

	/*
	 * <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the
	 * + sign on the left to edit the code.">
	 */
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request  servlet request
	 * @param response servlet response
	 * @throws ServletException if a servlet-specific error occurs
	 * @throws IOException      if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	} /* </editor-fold> */

}