package it.omicron.olimpiadi;

public class LanciatoreDisco extends Atleta {
	private Integer classifica;

	public LanciatoreDisco(String nome, String cognome, int id, Integer classifica) {
		super(nome, cognome, id, classifica);
		this.classifica = classifica;
	}

	public LanciatoreDisco() {
		super();
	}

	public LanciatoreDisco(Integer classifica) {
		super();
		this.classifica = classifica;
	}

	public Integer getClassifica() {
		return classifica;
	}

	public void setClassifica(Integer classifica) {
		this.classifica = classifica;
	}

	@Override
	public String toString() {
		return "LanciatoreDisco [classifica=" + classifica + "]";
	}

	@Override
	public int compareTo(Atleta ld) {
		// TODO Auto-generated method stub
		return this.classifica.compareTo(ld.getClassifica());
	}

}