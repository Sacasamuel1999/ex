package it.omicron.olimpiadi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Testing {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		ArrayList<ArrayList<Atleta>> ala = new ArrayList<ArrayList<Atleta>>();
		ArrayList<Atleta> ala1 = new ArrayList<Atleta>();
		// ArrayList<LanciatoreDisco> ald = new ArrayList<LanciatoreDisco>();
		ArrayList<Atleta> ala2 = new ArrayList<Atleta>();
		// ArrayList<Maratoneta> alm = new ArrayList<Maratoneta>();
		ArrayList<Atleta> ala3 = new ArrayList<Atleta>();
		System.out.println("1 - Iscrizione atleti");
		System.out.println("2 - Iscrizione lanciatori disco");
		System.out.println("3 - Iscrizione maratoneti");
		System.out.println("4 - Stampa atleti ordinata");
		System.out.println("5 - Stampa lanciatori del disco ordinata");
		System.out.println("6 - Stampa maratoneti ordinata");
		System.out.println("7 - Uscita");
		Testing t = new Testing();
		boolean exit = false;

		do {
			System.out.println("Inserisci un numero: ");
			int num = input.nextInt();

			switch (num) {
			case 1:
				t.iscrivi(ala1);
				break;
			case 2:
				t.iscriviLanciatoreDisco(ala2);
				break;
			case 3:
				t.iscriviMaratoneta(ala3);
				break;
			case 4:
				Collections.sort(ala1);
				for (Atleta atleta : ala1) {
					System.out.println(ala1);
				}
				break;
			case 5:
				Collections.sort(ala2);
				for (Atleta lanciatore_disco : ala2) {
					System.out.println(ala2);
				}
				break;
			case 6:
				Collections.sort(ala3);
				for (Atleta maratoneta : ala3) {
					System.out.println(ala3);
				}
				break;
			}
		} while (!exit);
		// ala.add(ala1);
		// ala.add(ala2);
		// ala.add(ala3);
	}

	public void iscrivi(List<Atleta> partecipanti) {
		try {
			Scanner input = new Scanner(System.in);
			System.out.println("Inserisci il numero di atleti: ");
			int na = input.nextInt();
			int count = 0;
			if (na == 0)
				throw new NumeroDiAtletiUgualeAZeroException();
			for (int i = 0; i < na; i++) {
				System.out.println("Inserisci il nome dell'atleta: ");
				String n = input.next();
				System.out.println("Inserisci il cognome dell'atleta: ");
				String c = input.next();
				System.out.println("Inserisci l'id dell'atleta: ");
				int id = input.nextInt();
				System.out.println("Inserisci la posizione in classifica dell'atleta: ");
				Integer classifica = input.nextInt();
				Atleta atleta = new Atleta(n, c, id, classifica);
				partecipanti.add(atleta);
				count++;
			}

		} catch (NumeroDiAtletiUgualeAZeroException e) {
			System.out.println(e.getMessage());
			iscriviDiNuovo();
		}
		System.out.println("L'iscrizione degli atleti � chiusa.");
	}

	public void iscriviDiNuovo() {
		Scanner input = new Scanner(System.in);
		ArrayList<ArrayList<Atleta>> ala = new ArrayList<ArrayList<Atleta>>();
		System.out.println("Inserisci il numero di atleti: ");
		int na = input.nextInt();
		int count = 0;
		if (na == 0) {
			System.out.println("Gli atleti iscritti continuano ad essere zero.");
			System.out.println("Poich� risultano zero atleti iscritti, le iscrizioni saranno chiuse!");
			System.out.println("L'iscrizione degli atleti � chiusa.");
			System.exit(0);
		}
		for (int i = 0; i < na; i++) {
			ArrayList<Atleta> ala1 = new ArrayList<Atleta>();
			System.out.println("Inserisci il nome dell'atleta: ");
			String n = input.next();
			System.out.println("Inserisci il cognome dell'atleta: ");
			String c = input.next();
			System.out.println("Inserisci l'id dell'atleta: ");
			int id = input.nextInt();
			System.out.println("Inserisci la posizione in classifica dell'atleta: ");
			Integer classifica = input.nextInt();
			ala.add(ala1);
			count++;
		}
	}

	public void iscriviLanciatoreDisco(List<Atleta> partecipanti) {
		try {
			Scanner input = new Scanner(System.in);
			System.out.println("Inserisci il numero dei lanciatori del disco: ");
			int nld = input.nextInt();
			int count = 0;
			if (nld > 2)
				throw new NumeroDiLanciatoriDelDiscoUgualeAZeroException();
			for (int i = 0; i < nld; i++) {
				System.out.println("Inserisci il nome del lanciatore del disco: ");
				String n = input.next();
				System.out.println("Inserisci il cognome del lanciatore del disco: ");
				String c = input.next();
				System.out.println("Inserisci l'id del lanciatore del disco: ");
				int id = input.nextInt();
				System.out.println("Inserisci la posizione in classifica del lanciatore del disco: ");
				Integer classifica = input.nextInt();
				Atleta lanciatore_disco = new Atleta(n, c, id, classifica);
				partecipanti.add(lanciatore_disco);
				count++;
			}
		} catch (NumeroDiLanciatoriDelDiscoUgualeAZeroException e) {
			System.out.println(e.getMessage());
			iscriviDiNuovoLanciatoreDisco();
		}
		System.out.println("L'iscrizione dei lanciatori del disco � chiusa.");
	}

	public void iscriviDiNuovoLanciatoreDisco() {
		Scanner input = new Scanner(System.in);
		ArrayList<ArrayList<Atleta>> ala = new ArrayList<ArrayList<Atleta>>();
		System.out.println("Inserisci il numero dei lanciatori del disco: ");
		int nld = input.nextInt();
		int count = 0;
		if (nld > 2) {
			System.out.println("I lanciatori del disco continuano ad essere pi� di due.");
			System.out.println("Poich� risultano pi� di due lanciatori del disco iscritti, la gara sar� annulata!");
			System.out.println("L'iscrizione dei lanciatori del disco � chiusa.");
			System.exit(0);
		} else if (nld == 0) {
			System.out.println("Risultano iscritti zero lanciatori del disco.");
			System.out.println("Poich� non risultano lanciatori del disco iscritti, la gara sar� annulata!");
			System.out.println("L'iscrizione dei lanciatori del disco � chiusa.");
		}
		for (int i = 0; i < nld; i++) {
			ArrayList<Atleta> ala2 = new ArrayList<Atleta>();
			System.out.println("Inserisci il nome del lanciatore del disco: ");
			String n = input.next();
			System.out.println("Inserisci il cognome del lanciatore del disco: ");
			String c = input.next();
			System.out.println("Inserisci l'id del lanciatore del disco: ");
			int id = input.nextInt();
			System.out.println("Inserisci la posizione in classifica del lanciatore del disco: ");
			Integer classifica = input.nextInt();
			ala.add(ala2);
			count++;
		}
	}

	public void iscriviMaratoneta(List<Atleta> partecipanti) {
		try {
			Scanner input = new Scanner(System.in);
			ArrayList<ArrayList<Atleta>> ala = new ArrayList<ArrayList<Atleta>>();
			System.out.println("Inserisci il numero di maratoneti: ");
			int nm = input.nextInt();
			int count = 0;
			if (nm > 4)
				throw new NumeroDiMaratonetiUgualeAZeroException();
			for (int i = 0; i < nm; i++) {
				System.out.println("Inserisci il nome del maratoneta: ");
				String n = input.next();
				System.out.println("Inserisci il cognome del maratoneta: ");
				String c = input.next();
				System.out.println("Inserisci l'id del maratoneta: ");
				int id = input.nextInt();
				System.out.println("Inserisci la posizione in classifica del maratoneta: ");
				Integer classifica = input.nextInt();
				Atleta maratoneta = new Atleta(n, c, id, classifica);
				partecipanti.add(maratoneta);
				count++;
			}
		} catch (NumeroDiMaratonetiUgualeAZeroException e) {
			System.out.println(e.getMessage());
			iscriviDiNuovoMaratoneta();
		}
		System.out.println("L'iscrizione dei maratoneti � chiusa.");
	}

	public void iscriviDiNuovoMaratoneta() {
		Scanner input = new Scanner(System.in);
		ArrayList<ArrayList<Atleta>> ala = new ArrayList<ArrayList<Atleta>>();
		System.out.println("Inserisci il numero dei maratoneti: ");
		int nm = input.nextInt();
		int count = 0;
		if (nm > 4) {
			System.out.println("I maratoneti continuano ad essere pi� di quattro.");
			System.out.println("Poich� risultano pi� di quattro maratoneti iscritti, la gara sar� annulata!");
			System.out.println("L'iscrizione dei maratoneti � chiusa.");
			System.exit(0);
		} else if (nm == 0) {
			System.out.println("Risultano iscritti zero maratoneti.");
			System.out.println("Poich� non risultano maratoneti iscritti, la gara sar� annulata!");
			System.out.println("L'iscrizione dei maratoneti � chiusa.");
			System.exit(0);
		}
		for (int i = 0; i < nm; i++) {
			ArrayList<Atleta> ala3 = new ArrayList<Atleta>();
			System.out.println("Inserisci il nome del maratoneta: ");
			String n = input.next();
			System.out.println("Inserisci il cognome del maratoneta: ");
			String c = input.next();
			System.out.println("Inserisci l'id del maratoneta: ");
			int id = input.nextInt();
			System.out.println("Inserisci la posizione in classifica del maratoneta: ");
			Integer classifica = input.nextInt();
			ala.add(ala3);
			count++;
		}
	}
}