package it.omicron.olimpiadi;

public class NumeroDiMaratonetiUgualeAZeroException extends Exception {
	public NumeroDiMaratonetiUgualeAZeroException() {
		super("I maratoneti non possono essere pi� di quattro! \n");
	}

	public NumeroDiMaratonetiUgualeAZeroException(String messaggio) {
		super(messaggio);
	}

}