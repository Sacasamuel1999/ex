package it.omicron.olimpiadi;

public class Maratoneta extends Atleta {
	private Integer classifica;

	public Maratoneta(String nome, String cognome, int id, Integer classifica) {
		super(nome, cognome, id, classifica);
		this.classifica = classifica;
	}

	public Maratoneta() {
		super();
	}

	public Maratoneta(Integer classifica) {
		super();
		this.classifica = classifica;
	}

	public Integer getClassifica() {
		return classifica;
	}

	public void setClassifica(Integer classifica) {
		this.classifica = classifica;
	}

	@Override
	public String toString() {
		return "Maratoneta [classifica=" + classifica + "]";
	}

	@Override
	public int compareTo(Atleta m) {
		// TODO Auto-generated method stub
		return this.classifica.compareTo(m.getClassifica());
	}

}