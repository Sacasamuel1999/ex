package it.omicron.olimpiadi;

public class Atleta implements Comparable<Atleta> {
	private String nome, cognome;
	int id;
	private Integer classifica;

	public Atleta() {
		super();
	}

	public Atleta(String nome, String cognome, int id, Integer classifica) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.id = id;
		this.classifica = classifica;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getClassifica() {
		return classifica;
	}

	public void setClassifica(Integer classifica) {
		this.id = classifica;
	}

	@Override
	public String toString() {
		return "Atleta [nome=" + nome + ", cognome=" + cognome + ", id=" + id + ", classifica=" + classifica + "]";
	}

	@Override
	public int compareTo(Atleta o) {
		// TODO Auto-generated method stub
		return this.classifica.compareTo(o.getClassifica());
	}

}