package it.omicron.olimpiadi;

public class NumeroDiLanciatoriDelDiscoUgualeAZeroException extends Exception {
	public NumeroDiLanciatoriDelDiscoUgualeAZeroException() {
		super("I lanciatori del disco non possono essere pi� di due! \n");
	}

	public NumeroDiLanciatoriDelDiscoUgualeAZeroException(String messaggio) {
		super(messaggio);
	}

}