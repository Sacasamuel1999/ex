package it.omicron.olimpiadi;

public class NumeroDiAtletiUgualeAZeroException extends Exception {
	public NumeroDiAtletiUgualeAZeroException() {
		super("Gli atleti iscritti non possono essere zero! \n");
	}

	public NumeroDiAtletiUgualeAZeroException(String messaggio) {
		super(messaggio);
	}

}