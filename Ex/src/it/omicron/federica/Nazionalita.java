package it.omicron.federica;

public enum Nazionalita {
	 ITALIA, FRANCIA, SPAGNA, RUSSIA, AMERICA, GERMANIA, CINA, GIAPPONE, AFRICA
}
