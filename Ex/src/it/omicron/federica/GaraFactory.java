package it.omicron.federica;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class GaraFactory extends Atleta {

	public static Gara creaGaraMaratona(GaraEnum maratona, List<Atleta> elencoPartecipanti,
			GregorianCalendar dataMaratona, Nazionalita spagna) {

		if (maratona.equals(GaraEnum.MARATONA)) {
			List<Maratoneta> maratoneti = new ArrayList<Maratoneta>();
			for (Atleta atleta : elencoPartecipanti) {
				if (atleta instanceof Maratoneta) {
					maratoneti.add((Maratoneta) atleta);
				}

			}
			return new GaraMaratona(maratona, maratoneti, dataMaratona);
		}

		return new Gara(null, elencoPartecipanti, dataMaratona, "");
	}

	public static Gara creaGaraLancio(GaraEnum lancio, List<Atleta> elencoPartecipanti, GregorianCalendar dataLancio,
			Nazionalita africa) {

		if (lancio.equals(GaraEnum.LANCIO_DEL_DISCO)) {
			List<LanciatoreDelDisco> lanciatori = new ArrayList<LanciatoreDelDisco>();
			for (Atleta atleta : elencoPartecipanti) {
				if (atleta instanceof LanciatoreDelDisco) {
					lanciatori.add((LanciatoreDelDisco) atleta);
				}

			}
			return new GaraLancio(lancio, lanciatori, dataLancio);
		}

		return new Gara(null, elencoPartecipanti, dataLancio, "");
	}

}