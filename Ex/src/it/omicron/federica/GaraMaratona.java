package it.omicron.federica;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

public class GaraMaratona extends Gara {
	List<Maratoneta> maratoneti;

	public GaraMaratona(GaraEnum maratona, List<Maratoneta> maratoneti, GregorianCalendar dataGara) {
		super(maratona, dataGara, "");
		this.maratoneti = maratoneti;
	}

	@Override
	public String getDescrizione() {
		return "NO! SONO UNA GARA MARATONA";
	}

	public List<Maratoneta> getElencoPartecipanti() {
		return maratoneti;
	}

}
