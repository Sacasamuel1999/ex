package it.omicron.federica;

import java.util.Scanner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class BackOffice {

	public static void main(String[] args) {
		List<Atleta> elencoPartecipanti = new ArrayList<Atleta>();

		Maratoneta maratoneta1 = new Maratoneta("Sara", "Fiocco", 222, Nazionalita.SPAGNA);
		elencoPartecipanti.add(maratoneta1);
		Maratoneta maratoneta2 = new Maratoneta("Lucia", "Petrucci", 896, Nazionalita.SPAGNA);
		elencoPartecipanti.add(maratoneta2);
		Maratoneta maratoneta3 = new Maratoneta("Martina", "Fiocco", 654, Nazionalita.SPAGNA);
		elencoPartecipanti.add(maratoneta3);
		Maratoneta maratoneta4 = new Maratoneta("Lucia", "Loop", 258, Nazionalita.SPAGNA);
		elencoPartecipanti.add(maratoneta4);
		LanciatoreDelDisco lanciatore1 = new LanciatoreDelDisco("James", "Sutton", 125, Nazionalita.SPAGNA);
		elencoPartecipanti.add(lanciatore1);
		LanciatoreDelDisco lanciatore2 = new LanciatoreDelDisco("Thomas", "Pilot", 225, Nazionalita.SPAGNA);
		elencoPartecipanti.add(lanciatore2);

		ArrayList<Atleta> elencoMaratoneti = new ArrayList<Atleta>();
		elencoMaratoneti.add(maratoneta1);
		elencoMaratoneti.add(maratoneta2);
		elencoMaratoneti.add(maratoneta3);
		elencoMaratoneti.add(maratoneta4);

		ArrayList<Atleta> elencoLanciatori = new ArrayList<Atleta>();
		elencoLanciatori.add(lanciatore1);
		elencoLanciatori.add(lanciatore2);

		GregorianCalendar dataMaratona = new GregorianCalendar(2019, 11, 10);
		GregorianCalendar dataLancio = new GregorianCalendar(2019, 11, 20);

		// FACTORY GARA
		GaraMaratona gara = (GaraMaratona) GaraFactory.creaGaraMaratona(GaraEnum.MARATONA, elencoPartecipanti,
				dataMaratona, Nazionalita.SPAGNA);
		System.out.println(gara.getDescrizione());
		System.out.println(gara.getElencoPartecipanti());
//		System.out.println("La Nazionalit� degli atleti �: " + Nazionalita.SPAGNA);

		GaraLancio gara2 = (GaraLancio) GaraFactory.creaGaraLancio(GaraEnum.LANCIO_DEL_DISCO, elencoPartecipanti,
				dataLancio, Nazionalita.AFRICA);
		System.out.println(gara2.getDescrizione());
		System.out.println(gara2.getElencoPartecipanti());
//		System.out.println("La Nazionalit� degli atleti �: " + Nazionalita.AFRICA);

//		List<Gara> elencoGare = new ArrayList<Gara>();
//		Gara maratona = new Gara("Maratona", elencoMaratoneti, dataMaratona);
//		elencoGare.add(maratona);
//		int i = 4;
//		for (Atleta m : elencoMaratoneti) {
//			m.setClassifica(i);
//			i--;
//		}
//		Gara lancioDisco = new Gara("Lancio del Disco", elencoLanciatori, dataLancio);
//		elencoGare.add(lancioDisco);
//
//		System.out.println("Benvenuti a tutti!");
//		System.out.println("Oggi vi presentiamo gli atleti:\n");
//		stampaPartecipanti(elencoPartecipanti);
//
//		System.out.println("\nLa Classifica definitiva dei Maratoneti �: \n");
//		Collections.sort(elencoMaratoneti);
//		stampaPartecipanti(elencoMaratoneti);

		List<String> list = new ArrayList<String>();

	}

	public static List<Atleta> getElencoPartecipanti(String tipoGara, List<Gara> elencoGare) {
		for (Gara gara : elencoGare) {
			if (gara.getGara().equals(tipoGara))
				return gara.getPartecipanti();

		}
		return null;
	}

	public static void stampaPartecipanti(List<Atleta> partecipanti) {
		for (Atleta str : partecipanti) {
			System.out.println(str);
		}
	}

	public static void iscrizioneAtleta() {
		Scanner tastiera = new Scanner(System.in);

	}
}
