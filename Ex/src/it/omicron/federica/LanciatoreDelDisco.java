package it.omicron.federica;

public class LanciatoreDelDisco extends Atleta {
	private String primoClassificato;
	private String secondoClassificato;

	public LanciatoreDelDisco() {
		super();
	}

	public LanciatoreDelDisco(String nome, String cognome, int id) {
		super(nome, cognome, id);
	}

	public LanciatoreDelDisco(int classifica, String primoClassificato, String secondoClassificato) {
		super();
		this.primoClassificato = primoClassificato;
		this.secondoClassificato = secondoClassificato;
	}

	public LanciatoreDelDisco(String string, String string2, int i, Nazionalita spagna) {
	}

	public String getPrimoClassificato() {
		return primoClassificato;
	}

	public void setPrimoClassificato(String primoClassificato) {
		this.primoClassificato = primoClassificato;
	}

	public String getSecondoClassificato() {
		return secondoClassificato;
	}

	public void setSecondoClassificato(String secondoClassificato) {
		this.secondoClassificato = secondoClassificato;
	}

	public void scriviOutput() {
		System.out.println("Nome: " + getNome());
		System.out.println("Cognome: " + getCognome());
		System.out.println("Id: " + getId());
		System.out.println("La nazionalit� �: " + getNazionalita());
	}

	/*
	 * public void setClassifica(String primoAtl, String secondoAtl) {
	 * this.primoClassificato = primoAtl; this.secondoClassificato = secondoAtl;
	 * System.out.println("IL VINCITORE E': " + primoAtl); System.out.println(); }
	 */

}
