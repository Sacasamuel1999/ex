package it.omicron.federica;

import java.util.GregorianCalendar;
import java.util.List;

public class GaraLancio extends Gara {
	List<LanciatoreDelDisco> lanciatoriDelDisco;

	public GaraLancio(GaraEnum lancio, List<LanciatoreDelDisco> lanciatoriDelDisco, GregorianCalendar dataGara) {
		super(lancio, dataGara, "");
		this.lanciatoriDelDisco = lanciatoriDelDisco;
	}

	@Override
	public String getDescrizione() {
		return "NO! SONO UNA GARA LANCIO DEL DISCO";

	}

	public List<LanciatoreDelDisco> getElencoPartecipanti() {
		return lanciatoriDelDisco;
	}

}
