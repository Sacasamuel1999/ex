package it.omicron.ex251;

import java.util.Scanner;

public class Ex251 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inserisci il numero delle righe e delle colonne: ");
		int num = input.nextInt();
		int K = num - 1;
		int M[][] = new int[num][num];
		int som_p = 0;
		int som_s = 0;
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.print("Inserire valori matrice: ");
				M[i][j] = input.nextInt();
			}
		}
		System.out.println(" ");
		System.out.println("Diagonale principale: ");
		for (int i = 0; i < num; i++) {
			System.out.println(M[i][i]);
			som_p = som_p + M[i][i];
		}
		System.out.println(" ");
		System.out.println("Diagonale secondaria: ");
		for (int i = 0; i < num; i++) {
			System.out.println(M[i][K]);
			som_s = som_s + M[i][K];
			K--;
		}
		System.out.println(" ");
		System.out.println("La somma della diagonale princpale �: " + som_p);
		System.out.println("La somma della diagonale secondaria �: " + som_s);
		System.out.println("");
		System.out.println("Sono maggiori i valori... ");
		if (som_p > som_s) {
			System.out.println("... della diagonale principale!");
		} else if (som_p < som_s) {
			System.out.println("... della diagonale secondaria!");
		} else if (som_p == som_s) {
			System.out.println("... di nessuna delle due.");
		}
	}
}