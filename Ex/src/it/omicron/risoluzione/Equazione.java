package it.omicron.risoluzione;

public class Equazione {
	public double a;
	public double b;
	public double c;
	public double delta;

	public Equazione(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public double get_Delta() {
		delta = (b * b) - 4 * a * c;
		return delta;
	}

	public void Controllo_Delta(double delta) {
		double x1, x2;
		if (delta > 0) {
			x1 = (-b + Math.sqrt(delta)) / (2 * a);
			x2 = (-b - Math.sqrt(delta)) / (2 * a);
			System.out.println("x1: " + x1 + " x2: " + x2);
		} else if (delta == 0) {
			x1 = (-b + Math.sqrt(delta)) / (2 * a);
			System.out.println("x1: " + x1);
		} else {
			System.out.println("Non ce Soluzione");
		}
	}
}