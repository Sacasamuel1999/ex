package it.omicron.risoluzione;

import java.util.Scanner;

public class Risoluzione {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il valore di A: ");
		double a = input.nextInt();
		System.out.println("Inserisci il valore di B: ");
		double b = input.nextInt();
		System.out.println("Inserisci il valore di C: ");
		double c = input.nextInt();
		Equazione eq = new Equazione(a, b, c);
		double delta = eq.get_Delta();
		eq.Controllo_Delta(delta);
	}
}