package it.omicron.usopersona;

import java.util.Date;
import java.util.GregorianCalendar;

public class Persona {
	private int id;
	private String nome;
	private Date data_nascita;
	GregorianCalendar data;

	public Persona(int id, String nome, int anno, int mese, int giorno) {
		this.id = id;
		this.nome = nome;
		data = new GregorianCalendar(anno, mese - 1, giorno);
		this.data_nascita = data.getTime();
	}

	public String toSring() {
		return id + " " + nome + " " + data_nascita;
	}

	public int Controllo_Eta() {
		return data_nascita.compareTo(data.getTime());

	}
}