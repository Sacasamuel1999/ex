package it.omicron.usoanagrafe;

import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class Anagrafe {
	private int id;
	private String nome;
	private String cognome;
	private String data;
	private String sesso;

	public Anagrafe() {
	}

	public Anagrafe(int id, String nome, String cognome, String data, String sesso) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.data = data;
		this.sesso = sesso;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
			this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
	this.cognome = cognome;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getSesso() {
		return sesso;
	}

	public int Controlla_Eta() {
		GregorianCalendar dat1 = new GregorianCalendar();
		GregorianCalendar dat2 = new GregorianCalendar();
		GregorianCalendar data1, data2;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		int anno, giorno, mese;
		String myData = getData();
		try {
			dat1.setTime(sdf.parse(myData));
		} catch (Exception e) {
			e.printStackTrace();
		}
		anno = dat1.get(GregorianCalendar.YEAR);
		mese = 1 + dat1.get(GregorianCalendar.MONTH);
		giorno = dat1.get(GregorianCalendar.DATE);
		data2 = new GregorianCalendar(2018, 02, 24); // 18/12/2008
		data1 = new GregorianCalendar(anno, mese, giorno);  
		int dif_anno = 2018 - anno;
		int dif_giorno, dif_mese;
		if (dif_anno == 18) {
			dif_mese = 3 - mese;
			if (dif_mese <= 0) {
				if (dif_mese == 0) {
					dif_giorno = 4 - giorno;
					if (dif_giorno < 0) {
						dif_anno = 17;
					}
				} else {
					dif_anno = 17;
				}
			}
		}
		if (dif_anno == 25) {
			dif_mese = 3 - mese;
			if (dif_mese <= 0) {
				if (dif_mese == 0) {
					dif_giorno = 4 - giorno;
					if (dif_giorno < 0) {
						dif_anno = 24;
					}
				} else {
					dif_anno = 24;
				}
			}
		}
		return dif_anno;
	}

	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
}