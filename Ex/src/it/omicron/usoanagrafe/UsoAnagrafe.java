package it.omicron.usoanagrafe;

import java.util.Scanner;

public class UsoAnagrafe {
	public static void main(String[] args) {
		Anagrafe A[] = new Anagrafe[5];
		Scanner input = new Scanner(System.in);
		Anagrafe A1;
		int sesso_m = 0, sesso_f = 0, cam_m = 0, cam_f = 0;
		for (int i = 0; i < A.length; i++) {
			System.out.println("Inserisci l'ID del residente: " + (i + 1) + "***");
			int id = input.nextInt();
			System.out.println("Inserisci il cognome del residente: ");
			String cognome = input.next();
			System.out.println("Inserisci il nome del residente: ");
			String nome = input.next();
			System.out.println("Inserisci la data del residente: ");
			String data = input.next();
			System.out.println("Inserisce il sesso del residente: ");
			String sesso = input.next();
			A[i] = new Anagrafe(id, cognome, nome, data, sesso);
			if (A[i].Controlla_Eta() >= 18) {
				if (A[i].Controlla_Eta() >= 25) {
					if (A[i].getSesso().equalsIgnoreCase("M")) {
						sesso_m++;
					} else {
						sesso_f++;
					}
				} else {
					if (A[i].getSesso().equalsIgnoreCase("M")) {
						cam_m++;
					} else {
						cam_f++;
					}
				}
			}
		}
		Anagrafe CameraM[] = new Anagrafe[cam_m + sesso_m];
		Anagrafe CameraF[] = new Anagrafe[cam_f + sesso_f];
		Anagrafe SenatoM[] = new Anagrafe[sesso_m];
		Anagrafe SenatoF[] = new Anagrafe[sesso_f];
		sesso_m = 0;
		sesso_f = 0;
		cam_m = 0;
		cam_f = 0;
		for (int i = 0; i < A.length; i++) {
			if (A[i].Controlla_Eta() >= 18) {
				if (A[i].Controlla_Eta() >= 25) {
					if (A[i].getSesso().equalsIgnoreCase("M")) {
						SenatoM[cam_m] = A[i];
						cam_m++;
					} else {
						SenatoF[cam_f] = A[i];
						cam_f++;
					}
				}
				if (A[i].Controlla_Eta() >= 18) {
					if (A[i].getSesso().equalsIgnoreCase("M")) {
						CameraM[sesso_m] = A[i];
						sesso_m++;
					} else {
						CameraF[sesso_f] = A[i];
						sesso_f++;
					}
				}
			}
		}
		Ordinare(CameraM);
		Ordinare(CameraF);
		Ordinare(SenatoM);
		Ordinare(SenatoF);
		Stampa(CameraM, "Camera Maschile");
		Stampa(CameraF, "Camera Femminile");
		Stampa(SenatoM, "Senato Maschile");
		Stampa(SenatoF, "Senato Femminile");
	}

	public static void Ordinare(Anagrafe v[]) {
		Anagrafe p;
		for (int i = 0; i < v.length - 1; i++) {
			for (int j = i + 1; j < v.length; j++) {
				if (v[i].getCognome().compareTo(v[j].getCognome()) > 0) {
					p = v[i];
					v[i] = v[j];
					v[j] = p;
				} else if (v[i].getCognome().compareToIgnoreCase(v[j].getCognome()) == 0) {
					if (v[i].getNome().compareTo(v[j].getNome()) > 0) {
						p = v[i];
						v[i] = v[j];
						v[j] = p;
					} else if (v[i].getNome().compareToIgnoreCase(v[j].getNome()) == 0) {
						if (v[i].Controlla_Eta() > v[j].Controlla_Eta()) {
							p = v[i];
							v[i] = v[j];
							v[j] = p;
						}
					}
				}
			}
		}
	}

	public static void Stampa(Anagrafe v[], String nome) {
		System.out.println("*****" + nome + "****");
		for (int i = 0; i < v.length; i++) {
			System.out.println(v[i]);
		}
	}
}