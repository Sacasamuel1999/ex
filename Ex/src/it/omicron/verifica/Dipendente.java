package it.omicron.verifica;

public class Dipendente {
	private String matricola;
	private String cognome;
	private String nome;
	private double pagaOraria;
	private int numeroFigli;
	private double stipendio;

	public Dipendente(String matricola, String cognome, String nome, double pagaOraria, int numeroFigli) {
		this.matricola = matricola;
		this.cognome = cognome;
		this.nome = nome;
		this.pagaOraria = pagaOraria;
		this.numeroFigli = numeroFigli;
	}

	public String getMatricola() {
		return this.matricola;
	}

	public String getCognome() {
		return this.cognome;
	}

	public String getNome() {
		return this.nome;
	}

	public double getPagaOraria() {
		return this.pagaOraria;
	}

	public int getNumeroFigli() {
		return this.numeroFigli;
	}

	public double getStipendio() {
		return this.stipendio;
	}

	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setPagaOraria(double pagaOraria) {
		this.pagaOraria = pagaOraria;
	}

	public void setNumeroFigli(int numeroFigli) {
		this.numeroFigli = numeroFigli;
	}

	public void setStipendio(double stipendio) {
		this.stipendio = stipendio;
	}

	@Override
	public String toString() {
		return "Dipendente [matricola=" + matricola + ", cognome=" + cognome + ", nome=" + nome + ", pagaOraria="
				+ pagaOraria + ", numeroFigli=" + numeroFigli + ", stipendio=" + stipendio + "]";
	}
}