package it.omicron.verifica;

import java.util.ArrayList;
import java.util.Random;

public class Verifica {
	public static void main(String[] args) {
		// l'array � dinamico, quindi accetta qualsiasi numero N di dipendenti.
		ArrayList<Dipendente> dipendenti = new ArrayList<>();
		String[] nomi = { "Mario", "Ciro", "Samuel", "Amedeo", "Davide" };
		String[] cognomi = { "Rossi", "Esposito", "Pino", "Avogadro", "Campagnolo" };
		Random random = new Random();
		for (int i = 0; i < nomi.length && i < cognomi.length; i++) {
			int pagaOraria = random.nextInt(50) + 1;
			int numeroFigli = random.nextInt(6) + 1;
			// istanziamento degli oggetti Dipendente nell'array.
			dipendenti.add(new Dipendente("M" + i, cognomi[i], nomi[i], (double) pagaOraria, numeroFigli));
			// calcolo dello stipendio.
			int assegniFamiliari = dipendenti.get(i).getNumeroFigli() * 50;
			double irpef = (dipendenti.get(i).getPagaOraria() * 160.0) * 20.0 / 100.0;
			double stipendioNetto = dipendenti.get(i).getPagaOraria() * 160 - irpef + assegniFamiliari;
			dipendenti.get(i).setStipendio(stipendioNetto);
		}
		double stipendioMedio = 0;
		int maxStipendio = 0;
		int f4 = 0;
		int f3 = 0;
		int f2 = 0;
		int f1 = 0;
		for (int i = 0; i < dipendenti.size(); i++) {
			// stampo i risultati.
			System.out.println("Nome: " + dipendenti.get(i).getNome() + "\nCognome: " + dipendenti.get(i).getCognome()
					+ "\nMatricola: " + dipendenti.get(i).getMatricola() + "\nStipendio Netto: "
					+ dipendenti.get(i).getStipendio());
			// somma gli stipendi netti.
			stipendioMedio += dipendenti.get(i).getStipendio();
			// trova il maggiore.
			if (dipendenti.get(i).getStipendio() > dipendenti.get(maxStipendio).getStipendio())
				maxStipendio = i;
			// conta le fasce.
			if (dipendenti.get(i).getStipendio() < 1000)
				f1++;
			else if (dipendenti.get(i).getStipendio() < 2000)
				f2++;
			else if (dipendenti.get(i).getStipendio() < 3000)
				f3++;
			else
				f4++;
		}
		// finisce il calcolo e stampa.
		System.out.println("Stipendio medio: " + (stipendioMedio / dipendenti.size()));
		// stampa il tizio con lo stipendio maggiore.
		System.out.println("Tizio con lo stipendio maggiore: \n");
		System.out.println("Nome: " + dipendenti.get(maxStipendio).getNome() + "\nCognome: "
				+ dipendenti.get(maxStipendio).getCognome() + "\nMatricola: "
				+ dipendenti.get(maxStipendio).getMatricola() + "\nStipendio Netto: "
				+ dipendenti.get(maxStipendio).getStipendio());
		System.out.println("\nStipendi 0-1000: " + f1);
		System.out.println("\nStipendi 1001-2000: " + f2);
		System.out.println("\nStipendi 2001-3000: " + f3);
		System.out.println("\nStipendi >3000: " + f4);
	}
}