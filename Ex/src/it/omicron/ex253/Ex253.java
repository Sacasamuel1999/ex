package it.omicron.ex253;

import java.util.Scanner;

public class Ex253 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inserisci il nummero delle righe e delle colonne: ");
		int num = input.nextInt();
		int M[][] = new int[num][num];
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.print("Inserisci i valori all'interno della matrice: ");
				M[i][j] = input.nextInt();
			}
		}
		System.out.println(" ");
		System.out.println("Matrice stampata in forma normale: ");
		for (int i = 0; i < num; i++) {
			for (int j = 0; j < num; j++) {
				System.out.print(M[i][j] + " ");
			}
		}
		System.out.println(" ");
		System.out.println("Matrice stampata per diagonale secondaria: ");
		for (int i = 0; i < num; i++) {
			int colonna = i;
			for (int riga = 0; riga < (i + 1); riga++) {
				System.out.print(M[riga][colonna] + " ");
				colonna--;
			}
		}
		for (int i = 1; i < num; i++) {
			int riga = i;
			for (int colonna = (num - 1); colonna > (i - 1); colonna--) {
				System.out.print(M[riga][colonna] + " ");
				riga++;
			}
		}
		int limiter = 1;
		int limitec = (num - 2);
		System.out.println(" ");
		System.out.println("Matrice stampata per diagonale principale: ");
		for (int i = (num - 1); i > (-1); i--) {
			int colonna = i;
			for (int riga = 0; riga < limiter; riga++) {
				System.out.print(M[riga][colonna] + " ");
				colonna++;
			}
			limiter++;
		}
		for (int i = 1; i < num; i++) {
			int riga = i;
			for (int colonna = 0; colonna < limitec; colonna++) {
				System.out.print(M[riga][colonna] + " ");
				riga++;
			}
			limitec--;
		}
	}
}