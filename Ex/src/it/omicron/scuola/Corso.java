package it.omicron.scuola;

public class Corso {
	private String codice, nome;
	private int livello, num_massimo, num_iscritti;

	public Corso(String codice, String nome, int livello, int num_massimo, int num_iscritti) {
		this.codice = codice;
		this.nome = nome;
		this.livello = livello;
		this.num_massimo = num_massimo;
		this.num_iscritti = num_iscritti;
	}

	public String getCodice() {
		return codice;
	}

	public String getNome() {
		return nome;
	}

	public int getLivello() {
		return livello;
	}

	public int getNumassimo() {
		return num_massimo;
	}

	public int getNuiscritti() {
		return num_iscritti;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setLivello(int livello) {
		this.livello = livello;
	}

	public void setNumassimo(int numassimo) {
		this.num_massimo = numassimo;
	}

	public void setNuiscritti(int nuiscritti) {
		this.num_iscritti = nuiscritti;
	}

	@Override
	public String toString() {
		return "Corso{" + "codice=" + codice + ", nome=" + nome + ", livello=" + livello + ", numassimo=" + num_massimo
				+ ", nuiscritti=" + num_iscritti + '}';
	}
}