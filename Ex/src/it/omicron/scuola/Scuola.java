package it.omicron.scuola;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Scuola {
	public static void main(String[] args) {
	      Corso[] alcorso = new Corso[5];
	      ArrayList<Corso> alc = new ArrayList();
	      HashMap<String, Corso> hm = new HashMap();
	      alcorso[0] = new Corso("A1", "Analisi 1", 1, 30, 30);
	      alcorso[1] = new Corso("G2", "Geometria 2", 2, 30, 23);
	      alcorso[2] = new Corso("POT", "Potenziamento", 3, 30, 30);
	      alcorso[3] = new Corso("J1", "Java1", 1, 30, 20);
	      alcorso[4] = new Corso("J2", "Java2", 3, 30, 19); 
	      Scanner sc = new Scanner(System.in);
	      System.out.println("Inserisci il livello {1-2-3}: ");
	      int l = sc.nextInt(); // riceve il livello
	      for(Corso c: alcorso){ // facciamo il "for each" - for(tipo variabile che riceve "corso", nome variabile 'c', da dove prendiamo l'oggetto "alcorso")
	          if(c.getLivello()==l){
	              alc.add(c);
	          } // condizione 
	          if(c.getNuiscritti()<c.getNumassimo()){
	              hm.put(c.getCodice(), c);
	          } // mettiamo la key e il valore dentro all'HashMap
	      }
	// for (int i=0; i < alc.size(); i++) { // for classico
	// System.out.println(alc);
	// }
	      for(Corso c: alc){
	          System.out.println(c);
	      }
	     for(Map.Entry<String, Corso> i: hm.entrySet()){ // Passaggio obbligatorio per entrare nell'HashMap, prendendo la key e il tipo, stampandolo poi succesivamente
	         String codice= i.getKey();
	         Corso c = i.getValue();
	         System.out.println(codice + " " + c);
	         
	     }
	    int nuncorso = alcorso.length;
	    double somma=0;
	     for(Corso c: alcorso) {
	        somma += c.getNuiscritti();
	     }
	        System.out.println("La media �: " + (somma/nuncorso));
	  }
	}