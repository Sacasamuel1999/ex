package it.omicron.doctor;

public class Surgeon extends Doctor {
	void treatPatient() {
		// perform surgery
	}

	void makeIncision() {
		// make incision (yikes!)
	}
}