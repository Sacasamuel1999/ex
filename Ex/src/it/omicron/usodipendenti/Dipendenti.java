package it.omicron.usodipendenti;

public class Dipendenti {
	private String Matricola, Cognome, Nome;
	private double PagaOraria;
	private int NumeroFigli;

	public Dipendenti() {
	}

	public Dipendenti(String Matricola, String Cognome, String Nome, double PagaOraria, int NumeroFigli) {
		this.Matricola = Matricola;
		this.Cognome = Cognome;
		this.Nome = Nome;
		this.PagaOraria = PagaOraria;
		this.NumeroFigli = NumeroFigli;
	}

	public String getMatricola() {
		return Matricola;
	}

	public String getCognome() {
		return Cognome;
	}

	public String getNome() {
		return Nome;
	}

	public double getPagaOraria() {
		return PagaOraria;
	}

	public int getNumeroFigli() {
		return NumeroFigli;
	}

	public void setMatricola(String Matricola) {
		this.Matricola = Matricola;
	}

	public void setCognome(String Cognome) {
		this.Cognome = Cognome;
	}

	public void setNome(String Nome) {
		this.Nome = Nome;
	}

	public void setPagaOraria(double PagaOraria) {
		this.PagaOraria = PagaOraria;
	}

	public void setNumeroFigli(int NumeroFigli) {
		this.NumeroFigli = NumeroFigli;
	}

	@Override
	public String toString() {
		return "Dipendenti{" + "Matricola=" + Matricola + ", Cognome=" + Cognome + ", Nome=" + Nome
				+ ", Stipendio Netto=" + Stipendio_Netto();
	}

	public double Stipendio_Netto() {
		int assegniFamiliari = NumeroFigli * 50;
		double IRPEF = (PagaOraria * 160) * 20 / 100;
		double StipendioNetto = PagaOraria * 160 - IRPEF + assegniFamiliari;
		return StipendioNetto;
	}
}