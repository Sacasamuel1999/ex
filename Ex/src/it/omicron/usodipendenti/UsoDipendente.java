package it.omicron.usodipendenti;

import java.util.Scanner;

public class UsoDipendente {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Inserisci il numero di dipendenti: ");
		int n = input.nextInt();
		Dipendenti D[] = new Dipendenti[n];
		for (int i = 0; i < D.length; i++) {
			System.out.println("Inserisci la matricola dei dipendenti: " + (i + 1));
			String matricola = input.next();
			System.out.println("Inserisci il cognome:");
			String cognome = input.next();
			System.out.println("Inserisci il nome: ");
			String nome = input.next();
			System.out.println("Inserisci la paga oraraia: ");
			double paga_oraria = input.nextDouble();
			System.out.println("Inserisci il numero di figli: ");
			int num_figli = input.nextInt();
			D[i] = new Dipendenti(matricola, cognome, nome, paga_oraria, num_figli);
		}
		for (int i = 0; i < D.length; i++) {
			System.out.println(D[i]);
		}
		double med = 0;
		for (int i = 0; i < D.length; i++) {
			med += D[i].Stipendio_Netto();
		}
		med /= D.length;
		System.out.println("La media dei dipendenti � uguale a: " + med);
		Dipendenti p;
		for (int i = 0; i < D.length - 1; i++) {
			for (int j = i + 1; j < D.length; j++) {
				if (D[i].Stipendio_Netto() < D[j].Stipendio_Netto()) {
					p = D[i];
					D[i] = D[j];
					D[j] = p;
				}
			}
		}
		double sti = D[0].Stipendio_Netto();
		System.out.println("Dati dei dipendenti con lo stipendio pi� alto: ");
		for (int i = 0; i < D.length; i++) {
			if (D[i].Stipendio_Netto() == sti) {
				System.out.println(
						"Dipendente:" + D[i].getCognome() + " " + D[i].getNome() + " " + D[i].Stipendio_Netto());
			}
		}
		int c1 = 0, c2 = 0, c3 = 0, c4 = 0;
		for (int i = 0; i < D.length; i++) {
			if (D[i].Stipendio_Netto() >= 0 && D[i].Stipendio_Netto() <= 1000) {
				c1++;
			}
			if (D[i].Stipendio_Netto() >= 1001 && D[i].Stipendio_Netto() <= 2000) {
				c2++;
			}
			if (D[i].Stipendio_Netto() >= 2001 && D[i].Stipendio_Netto() <= 3000) {
				c3++;
			}
			if (D[i].Stipendio_Netto() > 3000) {
				c4++;
			}
		}
		System.out.println("Fasce stipendio netto tra 0-1000: " + c1);
		System.out.println("Fasce stipendio netto tra 1001-2000: " + c2);
		System.out.println("Fasce stipendio netto tra 2001-3000: " + c3);
		System.out.println("Fasce stipendio netto oltre 3000: " + c4);
	}
}