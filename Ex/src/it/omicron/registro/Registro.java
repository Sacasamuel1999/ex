package it.omicron.registro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Registro {
	public static void main(String[] args) {
		ArrayList<Alunni> ala;
		HashMap<Integer, ArrayList> hm = new HashMap();
		HashMap<String, Integer> hm2 = new HashMap();
		Scanner sc = new Scanner(System.in);
		System.out.println("Inserisci il numero di giorni da segnare: ");
		int g = sc.nextInt();
		System.out.println("Inserisci il numero di alunni: ");
		int na = sc.nextInt();
		for (int i = 1; i <= g; i++) {
			ala = new ArrayList();
			System.out.println("Giorno: " + i);
			for (int j = 1; j < na; j++) {
				System.out.println("Inserisci il cognome dell'alunno: ");
				String c = sc.next();
				System.out.println("Inserisci il nome dell'alunno: ");
				String n = sc.next();
				System.out.println("Inserisci le ore di assenza: ");
				int o = sc.nextInt();
				ala.add(new Alunni(c, n, o));
			}
			hm.put(i, ala);
		}
		String cod;
		int k = 0;
		for (Map.Entry<Integer, ArrayList> i : hm.entrySet()) {
			g = i.getKey();
			ala = i.getValue();
			for (Alunni al : ala) {
				cod = al.getCognome() + "-" + al.getNome();
				int ore = al.getOre_assenza();
				if (hm2.containsKey(cod)) {
					ore += hm2.getOrDefault(cod, ore);
				}
				hm2.put(cod, ore);
			}
		}
		for (Map.Entry<String, Integer> i : hm2.entrySet()) {
			cod = i.getKey();
			na = i.getValue();
			System.out.println("Alunno: " + cod + " ore di assenza; " + na);

		}
	}
}