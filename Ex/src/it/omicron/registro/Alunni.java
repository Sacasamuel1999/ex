package it.omicron.registro;

import java.util.ArrayList;
import java.util.HashMap;

public class Alunni {
	private String nome, cognome;
	private int ore_assenza;

	public Alunni(String cognome, String nome, int ore_assenza) {
	        this.cognome = cognome;
	        this.nome=nome;
		this.ore_assenza = ore_assenza;
	    }

	@Override
	public String toString() {
		return " cognome: " + cognome + " ,nome: " + nome + ", ore di assenza: " + ore_assenza;
	}

	public String getNome() {
		return nome;
	}

	public String getCognome() {
		return cognome;
	}

	public int getOre_assenza() {
		return ore_assenza;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public void setOre_assenza(int ore_assenza) {
		this.ore_assenza = ore_assenza;
	}
}