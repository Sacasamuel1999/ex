package it.omicron.socket;

import java.io.*;
import java.net.*;

public class Client {
	public static void main(String[] args) throws IOException {
		String serverHostName = "127.0.0.1";

		if (args.length > 0)
			serverHostName = args[0];
		System.out.println("Attempting to connect... " + serverHostName + " on port 9000");

		Socket socket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		try {
			socket = new Socket(serverHostName, 9000);
			out = new PrintWriter(socket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			System.exit(1);
		}

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String userInput;

		System.out.println("Input client: ");
		while ((userInput = stdIn.readLine()) != null) {
			out.println(userInput);
			System.out.println("Output server: " + in.readLine());
			System.out.println("Input client: ");
			if (userInput.equals("Bye."))
				break;
		}

		out.close();
		in.close();
		stdIn.close();
		socket.close();
	}
}