package it.omicron.socket;

import java.net.*;
import java.io.*;

public class Server {
	public static void main(String[] args) throws IOException {
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(9000);
		} catch (Exception ex) {
			System.out.println("Couldn't listen on port 9000.");
			System.out.println(ex.getMessage());
			System.exit(1);
		}
		Socket clientSocket = null;
		System.out.println("Waiting for connection...");

		try {
			clientSocket = serverSocket.accept();
		} catch (Exception ex) {
			System.out.println("Accept failed!");
			System.out.println(ex.getMessage());
			System.exit(1);
		}

		System.out.println("Connection successfull!");
		System.out.println("Waiting for input...");

		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			if (inputLine.equals("Bye.")) {
				out.println("Bye, bye.");
				break;
			} else {
				int ric = Integer.parseInt(inputLine);
				double ris = Math.sqrt(ric);
				String m = " " + ris;
				out.println(m);
			}
		}

		out.close();
		in.close();
		clientSocket.close();
		serverSocket.close();
	}
}