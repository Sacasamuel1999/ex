package it.omicron.makehippo;

public abstract class Animal {
	private String name;

	public String getName() {
		return name;
	}

	public Animal(String theName) {
		name = theName;
	}
}