package it.omicron.ex250;

import java.util.Scanner;

public class Ex250 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("Inserisci l'indice delle celle del vettore: ");
		int cella = input.nextInt();
		int V[] = new int[cella];
		System.out.print("Inserisci il numero delle righe della matrice: ");
		int riga = input.nextInt();
		int colonna;
		for (int i = 0; i < cella; i++) {
			System.out.print("Inserisci i valori all'interno del vettore: ");
			V[i] = input.nextInt();
		}
		System.out.println(" ");
		System.out.println("Vettore: ");
		for (int i = 0; i < cella; i++) {
			System.out.println(V[i]);
		}
		if (cella % riga == 0) {
			colonna = (cella / riga);
		} else {
			colonna = (cella / riga) + 1;
		}
		int M[][] = new int[riga][colonna];
		int L = 0;
		for (int i = 0; i < riga; i++) {
			for (int j = 0; j < colonna; j++) {
				if (L < cella) {
					M[i][j] = V[L];
					L++;
				}
			}
		}
		System.out.println("");
		System.out.println("Matrice: ");
		for (int i = 0; i < riga; i++) {
			for (int j = 0; j < colonna; j++) {
				System.out.println(M[i][j]);
			}
		}
	}
}