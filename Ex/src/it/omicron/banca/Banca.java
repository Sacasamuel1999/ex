package it.omicron.banca;

public class Banca {
	public static void main(String[] args) {
		CC C1 = new CC("IT01", 100.000, "Samuel Pino");
		CC C2 = new CC();
		System.out.println(
				"Intestatario: " + C1.set_Intestatario() + " IABN: " + C1.set_Iban() + " Saldo: " + C1.set_Saldo());
		System.out.println(
				"Intestatario: " + C2.set_Intestatario() + " IABN: " + C2.set_Iban() + " Saldo: " + C2.set_Saldo());
		C2.get_Iban("IT02");
		C2.get_Intestatario("IT02");
		C2.get_Saldo(20.00);
		System.out.println(
				"Intestatario: " + C1.set_Intestatario() + " IABN: " + C1.set_Iban() + " Saldo: " + C1.set_Saldo());
		System.out.println(
				"Intestatario: " + C2.set_Intestatario() + " IABN: " + C2.set_Iban() + " Saldo: " + C2.set_Saldo());
		C2.get_Iban("IT02");
	}
}