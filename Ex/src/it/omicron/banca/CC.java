package it.omicron.banca;

public class CC {
	private String IBAN;
	private double Saldo;
	private String Intestatario;

	public CC(){   }

	public CC(String IBAN,double Saldo,String Intesttaario){ 
	        this.IBAN=IBAN;
	        this.Saldo=Saldo;
	        this.Intestatario=Intesttaario;
	    }

	public double Prelievo(double contanti) {
		Saldo -= contanti;
		return Saldo;
	}

	public double Versamento(double contanti) {
		Saldo += contanti;
		return Saldo;
	}

	public void get_Iban(String Iban) {
		this.IBAN = Iban;
	}

	public void get_Intestatario(String Intestatario) {
		this.Intestatario = Intestatario;
	}

	public void get_Saldo(double Saldo) {
		this.Saldo = Saldo;
	}

	public String set_Iban() {
		return IBAN;
	}

	public String set_Intestatario() {
		return Intestatario;
	}

	public double set_Saldo() {
		return Saldo;
	}
}